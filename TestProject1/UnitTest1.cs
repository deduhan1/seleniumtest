using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Collections.ObjectModel;
using TestProject1.Pages;
using TestProject1.Utilities;

namespace TestProject1;
[TestClass]
public class UnitTest1
{
    private IWebDriver _driver;
    private Utility _utility;

    #region Pages

    LoginPage _loginPage;
    BupaPage _bupaPage;

    #endregion

    [TestInitialize]
    public void Initialize()
    {
        _driver = new ChromeDriver();
        _loginPage = new LoginPage(_driver);
        _utility = new Utility(_driver);
        _bupaPage = new BupaPage(_driver);
    }

    [TestMethod]
    public void TestMethod1()
    {
        Assert.IsTrue(_loginPage?.Login());
    }

    [TestMethod]
    public void BupaPageTest()
    {
        Assert.IsTrue(_bupaPage?.Scenario());
    }

    [TestCleanup]
    public void Cleanup()
    {
        _driver?.Quit();
    }

    public bool fff()
    {


        ReadOnlyCollection<IWebElement> webElements = _driver.FindElements(By.ClassName("inventory_item"));

        var elements = webElements.Select(p => new {
            name = p.FindElements(By.ClassName("inventory_item_price"))[1].Text,
            price = p.FindElement(By.ClassName("inventory_item_name")).Text
        });

        var orderedElements = elements.OrderBy(p => p.name);

        return orderedElements == elements ? true : false;
    }
}