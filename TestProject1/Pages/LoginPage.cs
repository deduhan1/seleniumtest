﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestProject1.Utilities;

namespace TestProject1.Pages
{
    public class LoginPage : BasePage
    {
        private string _url = "https://www.saucedemo.com/";
        private string _userName = "standard_user";
        private string _password = "secret_sauce"; 

        public LoginPage(IWebDriver driver) : base(driver)
        {

        }

        #region Locators

        private IWebElement UserNameField => _driver.FindElement(By.Id("user-name"));
        private IWebElement PasswordField => _driver.FindElement(By.Id("password"));
        private IWebElement LoginButton => _driver.FindElement(By.Id("login-button"));
        private IWebElement Title => _driver.FindElement(By.ClassName("title"));

        #endregion

        #region Methods

        public bool Login()
        {
            _driver.Navigate().GoToUrl(_url);

            _wait.Until(p => UserNameField.Displayed);
            UserNameField.SendKeys(_userName);

            _wait.Until(p => PasswordField.Displayed);
            PasswordField.SendKeys(_password);

            _wait.Until(p => LoginButton.Displayed);
            LoginButton.xtClick();

            return _wait.Until(p => Title.Displayed);
        }

        #endregion
    }
}
