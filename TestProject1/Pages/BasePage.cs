﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject1.Pages
{
    public abstract class BasePage
    {
        protected IWebDriver _driver;
        protected WebDriverWait _wait;
        protected int _timeout = 10000;

        protected BasePage(IWebDriver driver)
        {
            _driver = driver;
            _wait = new WebDriverWait(_driver, TimeSpan.FromMilliseconds(_timeout));
        }

        protected IWebElement Wait(IWebElement element)
        {
            if (_wait.Until(p => element.Displayed))
                return element;

            throw new ArgumentNullException();
        }
    }
}
