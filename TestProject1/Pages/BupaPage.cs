﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestProject1.Utilities;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

namespace TestProject1.Pages
{
    public class BupaPage : BasePage
    {
        #region Parametrs

        private string _url = "https://www.bupa.co.uk/";
        private string _title = "Mr";
        private string _firstName = "Sergiej";
        private string _lastName = "Iliushenko";
        private string _email = "bupaTest@gmail.com";
        private string _phone = "777777777";
        private string _birthDay = "12";
        private string _birthMonth = "12";
        private string _birthYear = "2000";
        private string _postcode = "EC2R 7HJ";

        #endregion

        public BupaPage(IWebDriver driver) : base(driver)
        {
        }

        #region Elements

        private IWebElement CookiesButton => _driver.FindElement(By.Id("onetrust-accept-btn-handler"), 10);
        private IWebElement GetAQouteButton => _driver.FindElement(By.ClassName("btn-primary-alt"), 10);
        private IWebElement SubmitButton => _driver.FindElement(By.ClassName("submit-btn"), 10);
        private IWebElement TitleSelector => _driver.FindElement(By.Id("Prospect_ApplicantDetails_TitleCode"), 10);
        private IWebElement FirstNameField => _driver.FindElement(By.Id("Prospect_ApplicantDetails_FirstName"), 10);
        private IWebElement LastNameField => _driver.FindElement(By.Id("Prospect_ApplicantDetails_LastName"), 10);
        private IWebElement CoverSelector => _driver.FindElement(By.Id("Prospect_CoverDetail_CoverCode-button"), 10);
        private IWebElement CoverSelectorChoise => _driver.FindElement(By.Id("ui-id-2"), 10);
        private IWebElement DateOfBirthDayField => _driver.FindElement(By.Id("Prospect_ApplicantDetails_Day"), 10);
        private IWebElement DateOfBirthMonthField => _driver.FindElement(By.Id("Prospect_ApplicantDetails_Month"), 10);
        private IWebElement DateOfBirthYearField => _driver.FindElement(By.Id("Prospect_ApplicantDetails_Year"), 10);
        private IWebElement PostcodeField => _driver.FindElement(By.Id("Prospect_ApplicantDetails_ContactDetails_Postcode"), 10);
        private IWebElement AcceptCheckBox => _driver.FindElement(By.ClassName("terms-checkbox"), 10);
        private IWebElement ForstAdressFromDropDownBox => _driver.FindElement(By.ClassName("ui-menu-item"), 10);
        private IReadOnlyCollection<IWebElement> SmokeButtons => _driver.FindElements(By.ClassName("you-smoke-btn"), 10);
        private IWebElement PolicyField => _driver.FindElement(By.ClassName("policy-start-date-wrapper"), 10);
        private IWebElement PhoneNumberField => _driver.FindElement(By.Id("Prospect_ApplicantDetails_ContactDetails_Telephone"), 10);
        private IWebElement EmailField => _driver.FindElement(By.Id("Prospect_ApplicantDetails_ContactDetails_EmailAddress"), 10);

        #endregion

        #region Scenario

        public bool Scenario()
        {
            if (!GoToUrl())
                return false;

            CookiesButton.xtClick();

            GetAQouteButton.xtClick();

            SelectTitle();
            FirstNameField.SendKeys(_firstName);
            LastNameField.SendKeys(_lastName);

            SubmitButton.xtClick();

            CoverSelector.xtClick();
            CoverSelectorChoise.xtClick();

            SubmitButton.xtClick();

            DateOfBirthDayField.SendKeys(_birthDay);
            DateOfBirthMonthField.SendKeys(_birthMonth);
            DateOfBirthYearField.SendKeys(_birthYear);

            SubmitButton.xtClick();

            AcceptCheckBox.xtClick();
            PostcodeField.SendKeys(_postcode);
            ForstAdressFromDropDownBox.xtClick();

            SubmitButton.xtClick();

            SmokeButtons.ToList()[1].xtClick();

            SubmitButton.xtClick();

            _wait.Until(p => PolicyField.Displayed);

            SubmitButton.xtClick();

            PhoneNumberField.SendKeys(_phone);
            EmailField.SendKeys(_email);

            return true;
        }

        #endregion

        #region Methods

        private bool GoToUrl()
        {
            _driver.Navigate().GoToUrl(_url);
            
            return _driver.Url == _url ? true : false;
        }

        private void SelectTitle()
        {
            _wait.Until(p => TitleSelector.Displayed);

            var selectElement = new SelectElement(TitleSelector);
            selectElement.SelectByText(_title);
        }

        #endregion
    }
}
